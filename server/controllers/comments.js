// Get gravatar icon from email
const gravatar = require('gravatar')
// Get Comments model
const Comments = require('../models/comments')

// List comments
exports.list = (req, res) => {
  // List all comments and sort by Date
  Comments.find().sort('-created').populate('user', 'local.email').exec((error, comments) => {
    if(error) return res.send(400, { message: error })

    res.render('comments', {
      title: 'Comments Page',
      comments: comments,
      gravatar: gravatar.url(comments.email, {s: '80', r: 'x', d: 'retro'}, true)
    })
  })
}

// Create comments
exports.create = (req, res) => {
  // Create a new instance of the Comments model with request body
  const comments = new Comments(req.body)
  // Set current user id
  comments.user = req.user
  // Save data received
  comments.save(error => {
    if(error) return res.send(400, { message: error })
    res.redirect('/comments')
  })
}

// Comments authorization middleware
exports.hasAuthorization = (req, res, next) => {
  if (req.isAuthenticated()) return next()
  res.redirect('/login')
}