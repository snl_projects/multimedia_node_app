// Import basic modules
const express = require('express')
const path = require('path')
const favicon = require('serve-favicon')
const logger = require('morgan')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')

// Import Multer
const multer = require('multer')
const upload = multer({ dest: './public/uploads', limits: { fileSize: 1000000, files: 1 } })


/* PLACE FOR CONTROLLERS IMPORT */
// Import home controller
const index = require('./server/controllers/index')
// Import login controller
const auth = require('./server/controllers/auth')
// Import comments controller
const comments = require('./server/controllers/comments')
// Import videos controller
const videos = require('./server/controllers/videos')
// Import images controller
const images = require('./server/controllers/images')

/* PLACE FOR DATABASE CONFIGURATION */
// ODM with Mongoose
const mongoose = require('mongoose')
// Modules to store session
const session = require('express-session')
const MongoStore = require('connect-mongo')(session)
// Import Passport and Warning flash modules
const passport = require('passport')
const flash = require('connect-flash')
// Database configuration
var config = require('./server/config/config.js')
// connect to our database
mongoose.connect(config.url)
// Check if MongoDB is running
mongoose.connection.on('error', function() {
  console.error('MongoDB Connection Error. Make sure MongoDB is running.')
})
// Passport configuration
require('./server/config/passport')(passport)

/* PLACE FOR APP CONFIGURATION */
// start express application in variable app.
const app = express()

// view engine setup
app.set('views', path.join(__dirname, 'server/views/pages'))
app.set('view engine', 'ejs')
// middleware setup
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(require('node-sass-middleware')({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: true,
  sourceMap: true
}));

// Setup public directory
app.use(express.static(path.join(__dirname, 'public')))
// required for passport
// secret for session
app.use(session({
    secret: 'sometextgohere',
    saveUninitialized: true,
    resave: true,
    //store session on MongoDB using express-session + connect mongo
    store: new MongoStore({
    url: config.url,
    collection : 'sessions'
  })
}))

// Init passport authentication
app.use(passport.initialize())
// persistent login sessions
app.use(passport.session())
// flash messages
app.use(flash())

/* PLACE FOR APP ROUTES */
// Index Route
app.get('/', index.show)
app.get('/login', auth.signin)
app.post('/login', passport.authenticate('local-login', {
  //Success go to Profile Page / Fail go to login page
  successRedirect : '/profile',
  failureRedirect : '/login',
  failureFlash : true
}))
app.get('/signup', auth.signup)
app.post('/signup', passport.authenticate('local-signup', {
  //Success go to Profile Page / Fail go to Signup page
  successRedirect : '/profile',
  failureRedirect : '/signup',
  failureFlash : true
}))
app.get('/profile', auth.isLoggedIn, auth.profile)

// Logout Page
app.get('/logout', (req, res) => {
  req.logout()
  res.redirect('/')
})

// Setup routes for comments
app.get('/comments', comments.hasAuthorization, comments.list)
app.post('/comments', comments.hasAuthorization, comments.create)

// Setup routes for videos
app.get('/videos', videos.hasAuthorization, videos.show)
app.post('/videos', videos.hasAuthorization, upload.single('video'), videos.uploadVideo)

// Setup routes for images
app.post('/images', images.hasAuthorization, upload.single('image'), images.uploadImage)
app.get('/images-gallery', images.hasAuthorization, images.show)

/* PLACE FOR ERRORS HANDLING */
// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found')
  err.status = 404
  next(err)
})
// development error handler
// will print stacktrace
if(app.get('env') === 'development') {
  app.use((err, req, res, next) => {
    res.status(err.status || 500)
    res.render('error', {
      message: err.message,
      error: err
    })
  })
}

// production error handler
// no stacktraces leaked to user
app.use((err, req, res, next) => {
  res.status(err.status || 500)
  res.render('error', {
    message: err.message,
    error: {}
  })
})

module.exports = app;

app.set('port', process.env.PORT || 3000)
var server = app.listen(app.get('port'), () => {
  console.log('Express server listening on port ' + server.address().port)
})